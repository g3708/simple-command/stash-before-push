# stash-before-push



## Getting started

Stash sebelum melakukan push bertujuan menghindari auto merge atau conflict saat terjadi push ke server repository \
langkahnya sebagai berikut: 

1. Memastikan perubahan yang berada di server repository (jika tidak ada perubahan bisa lanjut ke langkah 5)
2. Menyimpan perubahan di lokal ke dalam stash
3. Menyamakan perubahan server repository dengan lokal
4. Menambahkan perubahan yang sebelumnya disimpan di dalam stash
5. Melakukan commit
6. Melakukan push

## command git yang digunakan
------ langkah 1 -------
```
git fetch
git status
```

------ langkah 2 -------
```
git stash
```

------ langkah 3 -------
```
git pull
```

------ langkah 4 -------
```
git stash pop 0
```

------ langkah 5 -------
```
git commit - m "YOUR_MESSAGE"
```

------ langkah 6 -------
```
git push <remote> <branch>
```
